#pragma once
class Camara
{
	int _x;
	int _y;
	int _w;
	int _h;
public:
	Camara();
	~Camara();

	void setX(int novaX);
	void setY(int novaY);
	void setW(int novaW);
	void setH(int novaH);

	int getX();
	int getY();
	int getH();
	int getW();

	void init();
	
};

