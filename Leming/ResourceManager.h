#pragma once
#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H
#include <iostream>
#include <map>
#include <vector>
#include<SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include<string>
//! ResourceManager class
/*!
	Handles the load and management of the graphics in the game.
*/
class ResourceManager
{
	public:

		//! Destructor.
		~ResourceManager();

		//! Deletes a graphic from the ResourceManager map
		/*!
			\param file Filepath to the graphic
		*/
		void removeGraphic(const char* file);


		//! Gets the graphic ID
		/*!
			\param file Filepath to the graphic
			\return ID of the graphic
		*/
		Sint32 getGraphicID(const char* file);


		//! Gets the graphic path given an ID graphic
		/*!
			\param ID of the graphic 
			\return Filepath to the graphic 
		*/
		std::string getGraphicPathByID(Sint32 ID);


		//! Returns width and Height of a Surface
		/*!
		 *	\param img ID texture
		 *	\param width Return variable for width value
		 *	\param height Return variable for height value
		 */
		void getGraphicSize(Sint32 img, int &width, int &height);

		//! Returns width of a Surface
		/*!
		 *	\param img ID texture
		 * 	\return Width of Texture
		 */
		Uint16 getGraphicWidth(Sint32 img);

		//! Returns Height of a Texture
		/*!
		 *	\param img ID texture
		 *  \return Height of Texture
		 */
		Uint16 getGraphicHeight(Sint32 img);

		//! Returns the SDL_Surface of the graphic
		/*!
			\param ID ID of the graphic
			\return SDL_Surface
		*/
		SDL_Texture* getGraphicByID(Sint32 ID);

		Mix_Chunk* getSoundByID(int ID);

		//! Change general Alpha value to paint a concrete surface
		/*!
			\param ID ID of the graphic
			\param alpha_value From SDL_ALPHA_TRANSPARENT(0) to SDL_ALPHA_OPAQUE(255)
		*/
		void setAlphaGraphic(Sint32 ID, Uint8 alpha_value);

		//! Prints the path to loaded graphics
		void printLoadedGraphics();

		//! Create a new surface graphic to the ResourceManager
		/*!
			\param name for the graphic
			\param width Width for the graphic
			\param height Height for the graphic
			\return -1 if there's an error when loading
		*/
		//Sint32 createGraphic(const char* name, Uint16 width, Uint16 height);
		Sint32 createTexture(const char* name, Uint16 width, Uint16 height);
		Sint32 createTexture(const char* name, Uint16 width, Uint16 height, Uint16 x, Uint16 y);

		int createSound(const char* name);

		void createMap(const char* name);
		SDL_Surface* getMap();
		void updateMap(int img);
		void renderMap();
		
		unsigned int getPixel(int x, int y);
		void putPixel(int x, int y, unsigned int pix);
		//! Gets Singleton instance
		/*!
			\return Instance of ResourceManager (Singleton).
		*/
		static ResourceManager* getInstance();

	protected:
		//! Constructor of an empty ResourceManager.
		ResourceManager();

	private:

		//! Adds a graphic to the ResourceManager 
		/*!
			\param file Filepath to the graphic
			\return -1 if there's an error when loading
		*/
		Sint32 addGraphic(const char* file);

		//! Searches in the ResourceManager and gets the graphic by its name. If it isn't there, loads it
		/*!
			\param file Filepath of the graphic
			\return SDL_Surface
		*/
		SDL_Texture* getGraphic(const char* file);

		//! Searches the first NULL in mGraphicsVector and updates mFirstFreeSlot to store its position
		/*!
			\return Index of the first NULL in mGraphicsVectorTexture
		*/
		Uint32 updateFirstFreeSlotGraphic();

		//SDL_Surface* loadSurface(std::string file);
		SDL_Texture* loadTexture(std::string file);
		SDL_Texture* loadTexture(std::string file, int x, int y, int h, int w);

		std::vector<SDL_Texture*>		mTexturesVector;	/*!<  Vector that stores Surfaces. Useful for sequential access*/
		//std::map<std::string, Sint32>		mIDMapt;
		//std::vector<SDL_Surface*>		mGraphicsVector;	/*!<  Vector that stores Surfaces. Useful for sequential access*/
		std::map<std::string, Sint32>		mIDMap;			/*!<  Map that stores ID. Links strings to ID, useful for sequential access*/
		Uint32					mFirstFreeSlot;		/*!<  First free slot in the mGraphicsVector*/

		SDL_Surface* _mapa;
		SDL_Texture*	mapTexture;
		static ResourceManager*			pInstance;		/*!<  Singleton instance*/
		void * mpixels;

		std::vector<Mix_Chunk*>  _mSoundVector;
};

#endif
