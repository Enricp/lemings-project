#include "Video.h"
Video* Video::unica_instancia = NULL;



Video::Video()
{
	gWindow = NULL;
	gScreenSurface = NULL;
	SDL_Init(SDL_INIT_EVERYTHING);
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 1024);
	Mix_AllocateChannels(128);
	gWindow = SDL_CreateWindow("Lemmings", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH*4, SCREEN_HEIGHT*4, SDL_WINDOW_SHOWN|SDL_WINDOW_FULLSCREEN_DESKTOP);
	
	renderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_RenderSetLogicalSize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT);
	_rsManager = ResourceManager::getInstance();
	gScreenSurface = SDL_GetWindowSurface(gWindow);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	_margeX = 0;
	_margeY = 0;
}


Video::~Video()
{
}

void Video::renderTexture(int img, int posX, int posY, int width, int height) {
	SDL_Rect r, rectAux;
	r.x = posX-_margeX;
	r.y = posY;
	r.w = width;
	r.h = height;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = 0;
	rectAux.y = 0;

	SDL_Texture *origin = _rsManager->getGraphicByID(img);
	SDL_RenderCopy(renderer, origin, &rectAux, &r);
	
}
void Video::renderTexture(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin)
{
	SDL_Rect r, rectAux;
	r.x = posX-_margeX;
	r.y = posY;
	r.w = width;
	r.h = height;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = posXorigin;
	rectAux.y = posYorigin;

	SDL_Texture *origin = _rsManager->getGraphicByID(img);
	SDL_RenderCopy(renderer, origin, &rectAux, &r);
}
void Video::renderTextureSO(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin)
{
	SDL_Rect r, rectAux;
	r.x = posX;
	r.y = posY;
	r.w = width;
	r.h = height;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = posXorigin;
	rectAux.y = posYorigin;

	SDL_Texture *origin = _rsManager->getGraphicByID(img);
	SDL_RenderCopy(renderer, origin, &rectAux, &r);
}
void Video::renderTextureReverse(int img, int posX, int posY, int width, int height)
{
	SDL_Rect r, rectAux;
	r.x = posX;
	r.y = posY;
	r.w = width;
	r.h = height;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = 0;
	rectAux.y = 0;

	SDL_Texture *origin = _rsManager->getGraphicByID(img);
	//SDL_RenderCopy(renderer, origin, &rectAux, &r);
	SDL_RenderCopyEx(renderer, origin, &rectAux, &r, 0, NULL, SDL_FLIP_HORIZONTAL);
}
void Video::renderTextureInverse(int img, int posX, int posY, int width, int height)
{
	SDL_Rect r, rectAux;
	r.x = posX;
	r.y = posY;
	r.w = width;
	r.h = height;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = 0;
	rectAux.y = 0;

	SDL_Texture *origin = _rsManager->getGraphicByID(img);
	//SDL_RenderCopy(renderer, origin, &rectAux, &r);
	SDL_RenderCopyEx(renderer, origin, &rectAux, &r, 0, NULL, SDL_FLIP_VERTICAL);
}
void Video::renderTextureReverse(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin)
{
	SDL_Rect r, rectAux;
	r.x = posX-_margeX;
	r.y = posY;
	r.w = width;
	r.h = height;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = posXorigin;
	rectAux.y = posYorigin;

	SDL_Texture *origin = _rsManager->getGraphicByID(img);
	//SDL_RenderCopy(renderer, origin, &rectAux, &r);
	SDL_RenderCopyEx(renderer, origin, &rectAux, &r, 0, NULL, SDL_FLIP_HORIZONTAL);
}
void Video::renderTextureReverseSO(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin)
{
	SDL_Rect r, rectAux;
	r.x = posX;
	r.y = posY;
	r.w = width;
	r.h = height;
	rectAux.h = height;
	rectAux.w = width;
	rectAux.x = posXorigin;
	rectAux.y = posYorigin;

	SDL_Texture *origin = _rsManager->getGraphicByID(img);
	//SDL_RenderCopy(renderer, origin, &rectAux, &r);
	SDL_RenderCopyEx(renderer, origin, &rectAux, &r, 0, NULL, SDL_FLIP_HORIZONTAL);
}
void Video::clearScreen(unsigned int color_key) {
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, 0, 0, 20, 255);
}
void Video::amagarMouse()
{
	SDL_ShowCursor(false);
}
void Video::updateScreen() {
	//SDL_UpdateWindowSurface(gWindow);

	SDL_RenderPresent(renderer);

}
void Video::renderMap()
{
	SDL_BlitSurface(_rsManager->getMap(), NULL, gScreenSurface, NULL);
}
void Video::waitTime(int ms) {
	SDL_Delay(ms);
}

void Video::controlaFrame(int frameRate)
{
	unsigned int lastTime = 0, currentTime, deltaTime;
	float msFrame = 1 / (frameRate / 1000.0f);
		currentTime = SDL_GetTicks();
	deltaTime = currentTime - lastTime;
	if (deltaTime < (int)msFrame) {
		SDL_Delay((int)msFrame - deltaTime);
	}
	lastTime = currentTime;
}

void Video::setMargeX(int noumargeX)
{
	_margeX = noumargeX;
}

void Video::setMargeY(int noumargeY)
{
	_margeY = noumargeY;
}

void Video::playSound(int id)
{
	Mix_PlayChannel(-1, _rsManager->getSoundByID(id), 0);
	

}

int Video::getMargeX()
{
	return _margeX;
}

void Video::close() {
	SDL_Quit();
}

int Video::frameControl(int oldTime)
{

	unsigned int  currentTime, deltaTime;
	float msFrame = 1 / (FPS / 1000.0f);
		currentTime = SDL_GetTicks();
	deltaTime = currentTime - oldTime;
	if (deltaTime < (int)msFrame) {
		SDL_Delay((int)msFrame-deltaTime);
	}
	oldTime = currentTime;
	return oldTime;
}
int Video::frameControl(int oldTime, int frameRate)
{
	unsigned int  currentTime, deltaTime;
	float msFrame = 1 / (frameRate / 1000.0f);
	currentTime = SDL_GetTicks();
	deltaTime = currentTime - oldTime;
	if (deltaTime < (int)msFrame) {
		SDL_Delay((int)msFrame - deltaTime);
	}
	oldTime = currentTime;
	return oldTime;
}
bool Video::vigilaFrames(int &oldTime, int frameRate)
{
	bool surt=true;
	unsigned int  currentTime, deltaTime;
	float msFrame = 1 / (frameRate / 1000.0f);
	currentTime = SDL_GetTicks();
	deltaTime = currentTime - oldTime;
	if (deltaTime < (int)msFrame) {
		surt = false;
	}
	else
	{
		oldTime = currentTime;
	}
	return surt;
}
bool Video::comprovarCollisio(int x1, int y1, int w1, int h1, int x2, int y2, int h2, int w2)
{
	bool col=false;
	
	SDL_Rect Primer;
	SDL_Rect Segon;
	Primer.x = x1;
	Primer.y = y1;
	Primer.h = h1;
	Primer.w = w1;

	Segon.x = x2;
	Segon.y = y2;
	Segon.h = h2;
	Segon.w = w2;
	SDL_Rect fin;
	col = SDL_IntersectRect(&Primer, &Segon, &fin);
	return col;
}
