#pragma once
#include "Video.h"

class Scene
{
public:
	Scene();
	~Scene();

	void init();
	void update();
	void render();
};

