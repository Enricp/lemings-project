#pragma once
#include "Video.h"

enum tipusBoto
{
	SUMA, RESTA, ESCALADOR, PARACAIGUDES, EXPLOTA, PARADOR, CONSTRUCTOR, PEGADOR, PICADOR, CAVADOR, VELOCITAT, MUERTE
};
class Boto
{
	bool _actiu;
	tipusBoto _tipus;
	int _img;
	int _icona;
	int _slot;
	int _numeros;
	int _x;
	int _y;
	int _w;
	int _h;
	int _usos;
	Video *_videoManager;
public:
	Boto();
	//Boto(tipusBoto nouTipus);
	~Boto();

	void setActiu(bool esActiu);
	void setTipus(tipusBoto nouTipus);
	void setImg(int novaImg);
	void setX(int novaX);
	void setY(int novaY);
	void setW(int novaW);
	void setH(int novaH);
	void setUsos(int nousUsos);

	int getImg();
	int getX();
	int getY();
	int getW();
	int getH();
	bool getActiu();
	tipusBoto getTipus();
	int getUsos();

	void init();
	void init(tipusBoto nouTipus);
	void update();
	void render();
};

