#include "MainMenu.h"



MainMenu::MainMenu()
{
}


MainMenu::~MainMenu()
{
}

void MainMenu::init()
{
	_videoManager = Video::getInstance();
	_img = _videoManager->_rsManager->createTexture("background.png", 320, 104, 0, 0);
	_jugar = new IconaMenu();
	_jugar->init(JUGAR);
	_sortir = new IconaMenu();
	_sortir->init(SORTIR);
}

void MainMenu::render()
{
	_videoManager->renderTextureSO(_img, 0, 0, 320, 104, 0, 0);
	_jugar->render();
	_sortir->render();
}
