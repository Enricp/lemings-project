#pragma once
#include <SDL.h>
#include <SDL_mixer.h>
#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 200
#include "ResourceManager.h"
#define FPS 60
class Video
{
	Video(void);
	static Video* unica_instancia;
	int _margeX;
	int _margeY;
	
public:
	ResourceManager*_rsManager;
	~Video();
	static Video *getInstance()
	{
		if (unica_instancia == NULL)
			unica_instancia = new Video();

		return unica_instancia;
	}
	
	void renderTexture(int img, int posX, int posY, int width, int height);
	void renderTexture(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin);//pinta nomes una part d'un grafic
	void renderTextureSO(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin);//pinta nomes una part d'un grafic sense camara
	void renderTextureReverse(int img, int posX, int posY, int width, int height);
	void renderTextureInverse(int img, int posX, int posY, int width, int height);
	void renderTextureReverse(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin);//pinta nomes una part d'un grafic
	void renderTextureReverseSO(int img, int posX, int posY, int width, int height, int posYorigin, int posXorigin);//pinta nomes una part d'un grafic
	void clearScreen(unsigned int color_key);
	void amagarMouse();
	void updateScreen();
	void renderMap();
	void waitTime(int ms);
	void controlaFrame(int frameRate);
	void setMargeX(int noumargeX);
	void setMargeY(int noumargeY);

	void playSound(int id);

	int getMargeX();

	void close();
	SDL_Window* gWindow;
	SDL_Surface* gScreenSurface;
	SDL_Renderer* renderer;
	int frameControl(int oldTime);//controla els frames
	int frameControl(int oldTime, int frameRate);//controla els frames
	bool vigilaFrames(int &oldTime, int frameRate);//controla els frames
	bool comprovarCollisio(int x1, int y1, int w1, int h1, int x2, int y2, int h2, int w2);
	
};


