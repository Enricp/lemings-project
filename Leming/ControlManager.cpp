#include "ControlManager.h"
ControlManager* ControlManager::unica_instancia = NULL;


ControlManager::ControlManager()
{
	 _mouseVerticalPosition =0;//posicio relativa d'y del ratoli
	 _exit=false;
	 _clic = false;
	 _keyControl=0; //0 quiet, 1 amun, 2 avall
}


ControlManager::~ControlManager()
{
}

void ControlManager::UpdateImput()
{
	SDL_PumpEvents();
	SDL_Event event;
	_clic = false;
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			_exit = true;
			break;

		case SDL_MOUSEMOTION:

			_mouseVerticalPosition = event.motion.y;
			_mouseHoritzonatlPosition = event.motion.x;
			break;
		case SDL_MOUSEBUTTONDOWN:
			_clic = true;
			break;
		}
	}
	
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_ESCAPE])
	{
		_exit = true;
	}else
	if (state[SDL_SCANCODE_1]) {
		_keyControl = 1;
	}
	else 	if (state[SDL_SCANCODE_2] ) {
		_keyControl = 2;
	}
	else 	if (state[SDL_SCANCODE_3]) {
		_keyControl = 3;
	}
	else 	if (state[SDL_SCANCODE_4]) {
		_keyControl = 4;
	}
	else 	if (state[SDL_SCANCODE_5]) {
		_keyControl = 5;
	}
	else 	if (state[SDL_SCANCODE_6]) {
		_keyControl = 6;
	}
	else 	if (state[SDL_SCANCODE_7]) {
		_keyControl = 7;
	}
	else 	if (state[SDL_SCANCODE_8]) {
		_keyControl = 8;
	}
	else 	if (state[SDL_SCANCODE_9]) {
		_keyControl = 9;
	}
	else 	if (state[SDL_SCANCODE_0]) {
		_keyControl = 10;
	}
	else 	if (state[SDL_SCANCODE_RIGHT]) {
		_keyControl = 20;
	}
	else 	if (state[SDL_SCANCODE_LEFT]) {
		_keyControl = 30;
	}
	else
	{
		_keyControl = 0;
	}

	/*if (SDL_QUIT)
	{
		SDL_Quit();
	}*/
}

float ControlManager::getMouseVerticalPosition()
{
	return _mouseVerticalPosition;
}


float ControlManager::getMouseHoritzontalPosition()
{
	return _mouseHoritzonatlPosition;
}

int ControlManager::getKeyControl()
{
	return _keyControl;
}

bool ControlManager::getExit()
{
	return _exit;
}

bool ControlManager::getClic()
{
	return _clic;
}
