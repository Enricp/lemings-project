#include "Boto.h"



Boto::Boto()
{
}


Boto::~Boto()
{
}

void Boto::setActiu(bool esActiu)
{
	_actiu = esActiu;
}

void Boto::setTipus(tipusBoto nouTipus)
{
	_tipus = nouTipus;
}

void Boto::setImg(int novaImg)
{
	_img = novaImg;
}

void Boto::setX(int novaX)
{
	_x = novaX;
}

void Boto::setY(int novaY)
{
	_y = novaY;
}

void Boto::setW(int novaW)
{
	_w = novaW;
}

void Boto::setH(int novaH)
{
	_h = novaH;
}

void Boto::setUsos(int nousUsos)
{
	_usos = nousUsos;
}

int Boto::getImg()
{
	return _img;
}

int Boto::getX()
{
	return _x;
}

int Boto::getY()
{
	return _y;
}

int Boto::getW()
{
	return _w;
}

int Boto::getH()
{
	return _h;
}

bool Boto::getActiu()
{
	return _actiu;
}

tipusBoto Boto::getTipus()
{
	return _tipus;
}

int Boto::getUsos()
{
	return _usos;
}

void Boto::init()
{
	 _actiu=false;
	 _tipus= SUMA;
	 _x = 0;
	 _y = 0;
	 _w = 16;
	 _h = 24;
	 _videoManager = Video::getInstance();
	 _img = _videoManager->_rsManager->createTexture("skill_panels.png", _w, _h, 0, 0);
//_icona= _videoManager->_rsManager->createTexture("skill_panels.png", 16, 23, 0, 0);
_slot = _videoManager->_rsManager->createTexture("empty_slot.png", 16, 23, 0, 0);
_numeros = _videoManager->_rsManager->createTexture("skill_count_digits.png", 56, 8, 0, 0);
	// _img=NULL;
	 _usos=10;
	//_videoManager= Video::getInstance();
}

void Boto::init(tipusBoto nouTipus)
{
	_actiu = false;
	_tipus = nouTipus;
	_x = 0;
	_y = 0;
	_w = 16;
	_h = 24;
	_videoManager = Video::getInstance();
	_img = _videoManager->_rsManager->createTexture("skill_panels.png", _w, _h, 0, 0);
	_slot = _videoManager->_rsManager->createTexture("empty_slot.png", 16, 23, 0, 0);
	_icona= _videoManager->_rsManager->createTexture("skill_panels.png", 16, 23, 0, 0);
	if (_tipus == VELOCITAT || _tipus == MUERTE) {
		//_slot = NULL;
		_numeros = NULL;
		if (_tipus == VELOCITAT) {
			_icona = _videoManager->_rsManager->createTexture("icon_pause.png", 16, 23, 0, 0);
		}
		else {
			_icona = _videoManager->_rsManager->createTexture("icon_nuke.png", 16, 23, 0, 0);
		}

	}
	else {

		
		_numeros = _videoManager->_rsManager->createTexture("skill_count_digits.png", 56, 8, 0, 0);
		switch (_tipus) {
		case SUMA:
			_icona = _videoManager->_rsManager->createTexture("icon_rr_plus.png", 16, 23, 0, 0);
			break;
		case RESTA:
			_icona = _videoManager->_rsManager->createTexture("icon_rr_minus.png", 16, 23, 0, 0);
			break;
		case ESCALADOR:
			_icona = _videoManager->_rsManager->createTexture("icon_climber.png", 16, 23, 0, 0);
			break;
		case PARACAIGUDES:
			_icona = _videoManager->_rsManager->createTexture("icon_floater.png", 16, 23, 0, 0);
			break;
		case EXPLOTA:
			_icona = _videoManager->_rsManager->createTexture("icon_bomber.png", 16, 23, 0, 0);
			break;
		case PARADOR:
			_icona = _videoManager->_rsManager->createTexture("icon_blocker.png", 16, 23, 0, 0);
			break;
		case CONSTRUCTOR:
			_icona = _videoManager->_rsManager->createTexture("icon_builder.png", 16, 23, 0, 0);
			break;
		case PEGADOR:
			_icona = _videoManager->_rsManager->createTexture("icon_basher.png", 16, 23, 0, 0);
			break;
		case PICADOR:
			_icona = _videoManager->_rsManager->createTexture("icon_miner.png", 16, 23, 0, 0);
			break;
		case CAVADOR:
			_icona = _videoManager->_rsManager->createTexture("icon_digger.png", 16, 23, 0, 0);
			break;

		default:
			_icona = NULL;
			break;
		}
		_usos = 15;

	}
}
void Boto::render()
{
	_videoManager->renderTextureSO(_img, _x, _y, _w, _h, 0, 0);
	if (_actiu)
	{
		_videoManager->renderTextureSO(_slot, _x, _y, _w, _h, 0, 0);
	}
	
	_videoManager->renderTextureSO(_icona, _x, _y, _w, _h, 0, 0);
	if(_tipus!=VELOCITAT&&_tipus!=MUERTE){
		//SDL_Rect actual;
		float hTall = 8;
		float wTall = 4;
		char  us1 = _usos%10;
		char  us2 = _usos/10 % 10;
		float xTall;
		switch (us1)
		{
		case 0:
			xTall = 0;
			break;
		case 1:
			xTall = 4.5f;
			break;
		case 2:
			xTall = 8.5f;
			break;
		case 3:
			xTall = 12.5f;
			break;
		case 4:
			xTall = 16.5f;
			break;
		case 5:
			xTall = 20.5f;
			break;
		case 6:
			xTall = 24.5f;
			break;
		case 7:
			xTall = 28.5f;
			break;
		case 8:
			xTall = 32.5f;
			break;
		case 9:
			xTall = 36.5f;
			break;
		default:
			break;
		}
		_videoManager->renderTextureSO(_numeros, _x+8, _y+2, wTall, hTall, 0, xTall);
		switch (us2)
		{
		case 0:
			xTall = 0;
			break;
		case 1:
			xTall = 4.5f;
			break;
		case 2:
			xTall = 8.5f;
			break;
		case 3:
			xTall = 12.5f;
			break;
		case 4:
			xTall = 16.5f;
			break;
		case 5:
			xTall = 20.5f;
			break;
		case 6:
			xTall = 24.5f;
			break;
		case 7:
			xTall = 28.5f;
			break;
		case 8:
			xTall = 32.5f;
			break;
		case 9:
			xTall = 36.5f;
			break;
		default:
			break;
		}
		_videoManager->renderTextureSO(_numeros, _x +2, _y+2, wTall, hTall, 0, xTall);
	}
}
