#pragma once
class Enrtrada
{
	int _x;
	int _y;
	int _h;
	int _w;
public:
	Enrtrada();
	~Enrtrada();

	int getX();
	int getY();
	int getH();
	int getW();

	void setX(int novaX);
	void setY(int novaY);
	void setW(int novaW);
	void setH(int novaH);
};

