#include "Sortida.h"



Sortida::Sortida()
{
}


Sortida::~Sortida()
{
}

int Sortida::getX()
{
	return _x;
}

int Sortida::getY()
{
	return _y;
}

int Sortida::getH()
{
	return _h;
}

int Sortida::getW()
{
	return _w;
}

void Sortida::setX(int novaX)
{
	_x = novaX;
}

void Sortida::setY(int novaY)
{
	_y = novaY;
}

void Sortida::setW(int novaW)
{
	_w = novaW;
}

void Sortida::setH(int novaH)
{
	_h = novaH;
}
