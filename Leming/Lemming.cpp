#include "Lemming.h"



Lemming::Lemming()
{
	init();
}


Lemming::~Lemming()
{
}

void Lemming::init()
{
	_x = 0;
	_y = 0;
	_feina = NATURAL;
	_dreta = true;
	_climber = false;
	_floater = false;
	_contadorAuxiliar = 0;
	_w = 16;
	_h = 16;
	_frameC = 0;
	_videoManager = Video::getInstance();
	_img = _videoManager->_rsManager->createTexture("lem.png", _w, _h, 0, 0);
}

void Lemming::update()
{
	if ( _videoManager->vigilaFrames(_frameC, 15))
	{

	
	switch (_feina)
	{
	case NATURAL:
		if (_dreta)
		{
			
			_x++;
		
			unsigned int s = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 8);
			unsigned int p = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 7);
			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 9);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			s = s & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			p = p & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			t = t & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if ((p != 0) && (t == 0))
			{
				_y-= 1;
				if (s != 0)
				{
					_y-=1;//torna a pujar un pixel si 
				}
			}
			if (t != 0) {
				
					_dreta=false;
				}
			if (t!=0&&_climber)
			{
				_feina = CLIMBER;
				_dreta = true;
			}
		
		}
		else
		{

			_x--;

			unsigned int s = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 8);
			unsigned int p = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 7);
			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 9);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			s = s & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			p = p & 0xFF000000;
			t = t & 0xFF000000;
			if (p != 0 && t == 0)
			{
				_y -= 1;
				if (s != 0)
				{
					_y -= 1;//torna a pujar un pixel si 
				}
			}
			if (t != 0) {

				_dreta = true;
			}
		}
		if (true)
		{


			//si es =0 esta buit i pot passar
			unsigned int i = _videoManager->_rsManager->getPixel(_x + _w / 2, _y + _h -5);
			unsigned int sc = _videoManager->_rsManager->getPixel(_x + _w / 2, _y + _h - 6);
			unsigned int c = _videoManager->_rsManager->getPixel(_x + _w / 2, _y + _h - 2);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			sc = sc & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			c = c & 0xFF000000;
			i = i & 0xFF000000;
			if (c == 0&&i==0)
			{
				_feina = CAIGUDA;
			}
			else {
				if (sc == 0)
				{
					_y += 1;

					if (_feina == CAIGUDA)
					{
						_feina = NATURAL;
					}
				}
			}
		}
			_frame++;
			if (_frame / 9 >= 9)
			{
				_frame = 0;
			}
		
		break;
	case BLOCKER:

		
	
		_frame++;
		if (_frame / 8 >= 16)
		{
			_frame = 0;
		}
		break;
	case BUILDER:

		if(_contadorAuxiliar<36)
		if (_dreta)
		{

			_x++;

			unsigned int s = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 8);
			unsigned int p = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 7);
			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 9);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			s = s & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			p = p & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			t = t & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if ((p != 0) && (t == 0))
			{
				_y -= 1;
				if (s != 0)
				{
					_y -= 1;//torna a pujar un pixel si 
				}
			}
			if (t != 0) {

				_dreta = false;
			}
			if (_contadorAuxiliar % 3 == 0) {
			for (int h = _y + _h-7 ; h < _y + _h-5; h++)
			{
				for (int j = _x+_w; j < _x +_w+ 6; j++)
				{
					_videoManager->_rsManager->putPixel(j, h, 0xFFFFFFFF);
				}

			}
			_videoManager->_rsManager->updateMap(0);
		}

		}
		else
		{

			_x--;

			unsigned int s = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 8);
			unsigned int p = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 7);
			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 9);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			s = s & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			p = p & 0xFF000000;
			t = t & 0xFF000000;
			if (p != 0 && t == 0)
			{
				_y -= 1;
				if (s != 0)
				{
					_y -= 1;//torna a pujar un pixel si 
				}
			}
			if (t != 0) {

				_dreta = true;
			}
		}
		_contadorAuxiliar++;
		if (_contadorAuxiliar==48)
		{
			_contadorAuxiliar = 0;
			_feina = NATURAL;
		}
		_frame++;
		if (_frame / 8 > 8)
		{
			_frame = 0;
		}
		break;
	case BASHER:
		if (_dreta)
		{
			unsigned int s = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 8);
			unsigned int p = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 7);
			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 9);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			s = s & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			p = p & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			t = t & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb

			_x++;
			//unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 9);
		//	t = t & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if (t != 0)
			{


				for (int h = _y - 2; h < _y + _h - 6; h++)
				{
					for (int j = _x; j < _x + _w - 5; j++)
					{
						_videoManager->_rsManager->putPixel(j, h, 0x00000000);
					}

				}
				_videoManager->_rsManager->updateMap(0);
			}
			else
			{
					_feina= NATURAL;				
			}

		}
		else {
			_x--;

			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 9);
			t = t & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if (t != 0)
			{

			for (int h = _y - 2; h < _y + _h - 6; h++)
			{
				for (int j = _x; j < _x + _w; j++)
				{
					_videoManager->_rsManager->putPixel(j, h, 0x00000000);
				}

			}
			_videoManager->_rsManager->updateMap(0);
		}
			else
			{
				_feina = NATURAL;			
			}

		}
		_frame++;
		if (_frame / 32 >= 32)
		{
			_frame = 0;
		}
		break;
	case MINER:
		_y++;
		if (_dreta)
		{
			_x++;
		

			unsigned int c = _videoManager->_rsManager->getPixel(_x + _w / 2, _y + _h -5);
			c = c & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if (c != 0)
			{


				for (int h = _y - 2; h < _y + _h - 7; h++)
				{
					for (int j = _x + 4; j < _x + _w - 2; j++)
					{
						_videoManager->_rsManager->putPixel(j, h, 0x00000000);
					}

				}
				_videoManager->_rsManager->updateMap(0);
			}
			else
			{
				_feina = NATURAL;
			}
			
			}
			else
			{
				_x--;
				
			}
				_frame++;
		if (_frame / 48 >= 24)
		{
			_frame = 0;
		}
		break;
	case DIGGER:
		_y++;
		if (true)
		{


			unsigned int c = _videoManager->_rsManager->getPixel(_x + _w / 2, _y + _h - 2);
			c = c & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if (c != 0)
			{


				for (int h = _y - 2; h < _y + _h - 7; h++)
				{
					for (int j = _x+4; j < _x + _w - 2; j++)
					{
						_videoManager->_rsManager->putPixel(j, h, 0x00000000);
					}

				}
				_videoManager->_rsManager->updateMap(0);
			}
			else
			{
				_feina = NATURAL;
			}
		}
		
		_frame++;
		if (_frame / 8 >= 8)
		{
			_frame = 0;
		}
		break;
	case CAIGUDA:
		_y++;
		if (true)
		{


			unsigned int sc = _videoManager->_rsManager->getPixel(_x + _w / 2, _y + _h - 6);
			unsigned int c = _videoManager->_rsManager->getPixel(_x + _w / 2, _y + _h - 2);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			sc = sc & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			c = c & 0xFF000000;
			if (c != 0)
			{

				if (sc == 0)
				{
					_y += 1;


					_feina = NATURAL;

				}
			}
		}
		_frame++;
		if (_frame / 4 >= 4)
		{
			_frame = 0;
		}


		break;
	case EXPLOTADOR:

		break;
	case CLIMBER:
		_y--;
		if (_dreta)
		{



			unsigned int s = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 8);
			unsigned int p = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 7);
			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) + 1, _y + _h - 9);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			s = s & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			p = p & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			t = t & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if ((p == 0))
			{
				_feina = NATURAL;
			}
		
		}
		else
		{
			unsigned int s = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 8);
			unsigned int p = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 7);
			unsigned int t = _videoManager->_rsManager->getPixel(_x + (_w / 2) - 1, _y + _h - 9);
			//s = s & 0x00FFFFFF;//aixo elimina el canal alfa i es queda amb els valors rgb
			s = s & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			p = p & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			t = t & 0xFF000000;//aixo elimina el canal alfa i es queda amb els valors rgb
			if ((p != 0) && (t == 0))
			{
				_feina = NATURAL;
			}
		}
			
		_frame++;
		break;
	case SORTINT:
		break;
	case FLOATER:
		_y++;
		_frame++;
		if (_frame / 8 >= 8)
		{
			_frame = 0;
		}
		break;
	case OFEGAT:
		break;
	default:
		break;
	}
}
}

void Lemming::render()
{
	int xTall;
	int yTall;
	switch (_feina)
	{
	case NATURAL:
		if (_dreta)
		{
			xTall = (_frame) / 9 * (_w + 4);
			_videoManager->renderTexture(_img, _x, _y, _w, _h, 0, xTall);

		}
		else
		{
			xTall = (_frame) / 9 * (_w + 4);
			_videoManager->renderTextureReverse(_img, _x, _y, _w, _h, 0, xTall);
		}
	
		break;
	case BLOCKER:
		xTall = (_frame) / 8 * (_w + 4);
		
		_videoManager->renderTexture(_img, _x, _y, _w, _h, 60, xTall);
		
		break;
	case BUILDER:
	xTall=(_frame)/8*(_w+4);
	if(_dreta)
	{
	_videoManager->renderTexture(_img,_x,_y,_w,_h,100,xTall);
	}
	else
	{
		_videoManager->renderTextureReverse(_img,_x,_y,_w,_h,100,xTall);
	}
		break;
	case BASHER:
		yTall = 0;
		xTall = (_frame) / 32 * (_w + 4);
		if (xTall>=1088)
		{
			xTall -= 1088;
			yTall = 20;
		}
		//std::cout <<xTall << std::endl;
		_videoManager->renderTexture(_img, _x, _y, _w, _h, 120+yTall, xTall);
		break;
	case MINER:
		yTall=0;
		xTall = (_frame) / 8 * (_w + 4);
			if (xTall>=1088)
			{
				xTall -= 1088;
				yTall = 20;
			}

 			_videoManager->renderTexture(_img, _x, _y , _w, _h,180+yTall,xTall );
		break;
	case DIGGER:
		
		xTall = (_frame) / 8 * (_w + 4);
	
		//std::cout <<xTall << std::endl;
		_videoManager->renderTexture(_img, _x, _y, _w, _h, 160, xTall);
		break;
	case CAIGUDA:
	xTall=(_frame)/4*(_w+4);
	_videoManager->renderTexture(_img,_x,_y,_w,_h,40, xTall);
	break;
	case EXPLOTADOR:
	break;
	case CLIMBER:
		xTall = (_frame) / 14 * (_w + 4);

		_videoManager->renderTexture(_img, _x, _y, _w, _h, 80, xTall);
	break;
	case SORTINT:
	break;
	case FLOATER:
	xTall=(_frame)/8*(_w+4);
	_videoManager->renderTexture(_img,_x, _y,_w,_h,40,xTall); //no es te en compte l'offset ni que l'animacio es fixe
	break;
	case OFEGAT:
	break;
	default:
		break;
	}
	
}

void Lemming::setImg(int novaImg)
{
	_img = novaImg;
}

void Lemming::setX(int novaX)
{
	_x = novaX;
}

void Lemming::setY(int novaY)
{
	_y = novaY;
}

void Lemming::setW(int novaW)
{
	_w = novaW;
}

void Lemming::setH(int novaH)
{
	_h = novaH;
}

void Lemming::setFeina(Feina novaFeina)
{
	_feina = novaFeina;
}

void Lemming::setClimber(bool clim)
{
	_climber = clim;
}

void Lemming::setFloater(bool flot)
{
	_floater = flot;
}

void Lemming::setDreta(bool dreta)
{
	_dreta = dreta;
}

void Lemming::setContador(int nouContador)
{
	_contadorAuxiliar = nouContador;
}



int Lemming::getImg()
{
	return _img;
}

int Lemming::getX()
{
	return _x;
}

int Lemming::getY()
{
	return _y;
}

int Lemming::getW()
{
	return _w;
}

int Lemming::getH()
{
	return _h;
}

Feina Lemming::getFeina()
{
	return _feina;
}

bool Lemming::getClimber()
{
	return _climber;
}

bool Lemming::getFloater()
{
	return _floater;
}

bool Lemming::getDreta()
{
	return _dreta;
}

int Lemming::getContador()
{
	return _contadorAuxiliar;
}


