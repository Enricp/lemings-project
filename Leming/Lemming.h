#pragma once
#include "Video.h"

enum Feina
{
	NATURAL,BLOCKER,BUILDER,BASHER,MINER, DIGGER, CAIGUDA, EXPLOTADOR, CLIMBER, SORTINT, FLOATER,OFEGAT
};
class Lemming
{
	int _img;
	int _x;
	int _y;
	int _w;
	int _contadorAuxiliar;
	int _frameC;
	Feina _feina;
	int _h;
	bool _climber;
	bool _floater;
	bool _dreta;
	int _frame;
	Video *_videoManager;
public:
	Lemming();
	~Lemming();

	void init();
	void update();
	void render();


	void setImg(int novaImg);
	void setX(int novaX);
	void setY(int novaY);
	void setW(int novaW);
	void setH(int novaH);
	void setFeina(Feina novaFeina);
	void setClimber(bool clim);
	void setFloater(bool flot);
	void setDreta(bool dreta);
	void setFrame(int nouFrame);
	void setContador(int nouContador);

	int getImg();
	int getX();
	int getY();
	int getW();
	int getH();
	Feina getFeina();
	bool getClimber();
	bool getFloater();
	bool getDreta();
	int getFrame();
	int getContador();
};

