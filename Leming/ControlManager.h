#pragma once
#include <SDL.h>
#include<iostream>
class ControlManager
{
	ControlManager(void);
	static ControlManager* unica_instancia;
	float _mouseVerticalPosition;//posicio  d'y del ratoli
	float _mouseHoritzonatlPosition;
	bool _clic;
	bool _exit;
	int _keyControl; //0 quiet, 1 dreta, 2 esquerre
public:

	~ControlManager();
	static ControlManager *getInstance()
	{
		if (unica_instancia == NULL)
			unica_instancia = new ControlManager();

		return unica_instancia;
	}

	void UpdateImput();
	float getMouseVerticalPosition();
	float getMouseHoritzontalPosition();
	int getKeyControl();
	bool getExit();
	bool getClic();
};

