#pragma once
#include "Video.h"
enum EstatPunter
{
	NORMAL,SOBRE
};
class Punter
{

	int _img;
	int _x;
	int _y;
	int _w;
	EstatPunter _estat;
	int _h;
	Video *_videoManager;
public:
	Punter();
	~Punter();

	void init();
	void update();
	void render();


	void setImg(int novaImg);
	void setX(int novaX);
	void setY(int novaY);
	void setW(int novaW);
	void setH(int novaH);
	void setEstat(EstatPunter nouEstat);

	int getImg();
	int getX();
	int getY();
	int getW();
	int getH();
	int getCentreX();
	int getCentreY();
	EstatPunter getEstat();
};

