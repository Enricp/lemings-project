#include "Punter.h"



Punter::Punter()
{
	init();
}


Punter::~Punter()
{
}

void Punter::init()
{
	_x = 0;
	_y = 0;
	_estat = NORMAL;
	_w = 16;
	_h = 16;
	_videoManager = Video::getInstance();
	_img=_videoManager->_rsManager->createTexture("lem.png",_w,_h,20,277);
}

void Punter::render()
{
	if (_estat == NORMAL)
	{
		_videoManager->renderTextureSO(_img, _x, _y, _w, _h, 280, 20);
	}
	if (_estat==SOBRE)
	{
		_videoManager->renderTextureSO(_img, _x, _y, _w, _h, 280, 0);

	}
}

void Punter::setImg(int novaImg)
{
	_img = novaImg;
}

void Punter::setX(int novaX)
{
	_x = novaX;
}

void Punter::setY(int novaY)
{
	_y = novaY;
}

void Punter::setW(int novaW)
{
	_w = novaW;
}

void Punter::setH(int novaH)
{
	_h = novaH;
}

void Punter::setEstat(EstatPunter nouEstat)
{
	_estat = nouEstat;
}

int Punter::getImg()
{
	return _img;
}

int Punter::getX()
{
	return _x;
}

int Punter::getY()
{
	return _y;
}

int Punter::getW()
{
	return _w;
}

int Punter::getH()
{
	return _h;
}

int Punter::getCentreX()
{
	return _x+_w/2;
}

int Punter::getCentreY()
{
	return _y+_h/2;
}

EstatPunter Punter::getEstat()
{
	return _estat;
}
