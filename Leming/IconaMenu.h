#pragma once
#include "Video.h"

enum tipusBotoMenu
{
	SORTIR, JUGAR
};

class IconaMenu
{
	int _img;
	int _x;
	int _y;
	int _w;
	int _h;
	tipusBotoMenu _tipus;

	Video* _videoManager;

public:
	IconaMenu();
	~IconaMenu();

	void setImg(int novaImg);
	void setX(int novaX);
	void setY(int novaY);
	void setH(int novaH);
	void setW(int novaW);
	void setTipus(tipusBotoMenu nouTipus);

	int getImg();
	int getX();
	int getY();
	int getH();
	int getW();
	tipusBotoMenu getTipus();
	
	void init();
	void init(tipusBotoMenu tipus);
	void render();

};

