#include "ResourceManager.h"
#include "Video.h"


ResourceManager* ResourceManager::pInstance = NULL;

ResourceManager::~ResourceManager()
{
}

void ResourceManager::removeGraphic(const char * file)
{

	Sint32 num = mIDMap[file];
	SDL_DestroyTexture(mTexturesVector[num]);
	mTexturesVector[num] = NULL;
	mIDMap.erase(file);

}

Sint32 ResourceManager::getGraphicID(const char * file)
{
	Sint32 res;
	if (mIDMap.find(file) != mIDMap.end()) {
		res = mIDMap[file];
	}
	else
	{
		mTexturesVector.push_back( loadTexture(file));
		mIDMap.insert(std::pair<std::string, Sint32>(file, (mTexturesVector.size()-1)));
		res = mTexturesVector.size() - 1;
	}
	return res;
}

std::string ResourceManager::getGraphicPathByID(Sint32 ID)
{
	std::string resut;
	for (std::map<std::string, Sint32>::iterator it = mIDMap.begin(); it != mIDMap.end(); ++it) {
		if (it->second == ID) {
			resut =it->first;
		}
	}
	
	return resut;
}

void ResourceManager::getGraphicSize(Sint32 img, int & width, int & height)
{
	if (mTexturesVector.size() <img)
	{
		
		SDL_QueryTexture(mTexturesVector[img],NULL,NULL,&width,&height);
	}

}

Uint16 ResourceManager::getGraphicWidth(Sint32 img)
{
	int w;
	SDL_QueryTexture(mTexturesVector[img], NULL, NULL, &w, NULL);
	return w;
}

Uint16 ResourceManager::getGraphicHeight(Sint32 img)
{
	int h;
	SDL_QueryTexture(mTexturesVector[img], NULL, NULL, NULL, &h);
	return 	h;
}


Sint32 ResourceManager::createTexture(const char * name, Uint16 width, Uint16 height)
{
	Sint32 res;
	if (mIDMap.find(name) != mIDMap.end()) {
		res = mIDMap[name];
	//	res = -1;
	}
	else
	{
		mTexturesVector.push_back(loadTexture(name));
		mIDMap.insert(std::pair<std::string, Sint32>(name, (mTexturesVector.size() - 1)));
		res = mTexturesVector.size() - 1;
	}
	return res;
}

Sint32 ResourceManager::createTexture(const char * name, Uint16 width, Uint16 height, Uint16 x, Uint16 y)
{
	Sint32 res;
	if (mIDMap.find(name) != mIDMap.end()) {
		res = mIDMap[name];
		//res = -1;
	}
	else
	{
		mTexturesVector.push_back(loadTexture(name,x,y,height,width));
		mIDMap.insert(std::pair<std::string, Sint32>(name, (mTexturesVector.size() - 1)));
		res = mTexturesVector.size() - 1;
		
	}
	return res;
}

int ResourceManager::createSound(const char * name)
{

	Mix_Chunk* sound = Mix_LoadWAV(name);
	_mSoundVector.push_back(sound);
	int res =(int) _mSoundVector.size();
	res - 1;
	return res;
}

void ResourceManager::createMap(const char * name)
{

	std::string path = name; SDL_Surface *surfaceFormatInfo = Video::getInstance()->gScreenSurface;



	//The final optimized image
	//SDL_Surface* optimizedSurface = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	/*if (loadedSurface == NULL)
	{
		printf("Unable to load image ", path.c_str(), "! SDL_image Error: ", IMG_GetError());
	}
	else
	{
		//Convert surface to screen format
		// Save Alpha values from surface info
		Uint32 Old_Amask = surfaceFormatInfo->format->Amask;
		Uint32 Old_Aloss = surfaceFormatInfo->format->Aloss;
		Uint32 Old_Ashift = surfaceFormatInfo->format->Ashift;
		// Force Alpha values
		surfaceFormatInfo->format->Amask = 0xFF000000;
		surfaceFormatInfo->format->Aloss = 0;
		surfaceFormatInfo->format->Ashift = 24;
		// Convert to screen format
		optimizedSurface = SDL_ConvertSurface(loadedSurface, surfaceFormatInfo->format, NULL);
		// Restore alpha values for surface info
		surfaceFormatInfo->format->Amask = Old_Amask;
		surfaceFormatInfo->format->Aloss = Old_Aloss;
		surfaceFormatInfo->format->Ashift = Old_Ashift;
		if (optimizedSurface == NULL)
		{
			printf("Unable to optimize image ", path.c_str(), "! SDL Error: ", SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}*/
	_mapa = loadedSurface;
	//SDL_SetColorKey(_mapa, SDL_TRUE, SDL_MapRGB(_mapa->format, 0, 0xFF, 0xFF));
	//SDL_Surface* formattedSurface = SDL_ConvertSurface(loadedSurface, SDL_GetWindowSurface(Video::getInstance()->gWindow)->format, NULL);
	//memcpy(pixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h);
	
	mapTexture = SDL_CreateTexture(Video::getInstance()->renderer, loadedSurface->format->format, SDL_TEXTUREACCESS_STREAMING, loadedSurface->w, loadedSurface->h);
	if (mapTexture == NULL)
	{
		printf("Unable to optimize image ", path.c_str(), "! SDL Error: ", SDL_GetError());
	}
	//SDL_LockTexture(mapTexture, NULL, &loadedSurface->pixels,&loadedSurface->pitch);
	 
	// pixels = _mapa->pixels;
	//Copy loaded/formatted surface pixels
	//memcpy(mpixels, loadedSurface->pixels, loadedSurface->pitch * loadedSurface->h);

	SDL_UpdateTexture(mapTexture, NULL, loadedSurface->pixels, loadedSurface->pitch);
	//Unlock texture to update
	//SDL_UnlockTexture(mapTexture);
	//_mapa = formattedSurface;
//	memset(pixels, 255, _mapa->w * _mapa->h * sizeof(Uint32));

}

SDL_Surface* ResourceManager::getMap()
{
	return _mapa;
}

void ResourceManager::updateMap(int img)
{
	//SDL_Surface* formattedSurface = SDL_ConvertSurface(_mapa, SDL_GetWindowSurface(Video::getInstance()->gWindow)->format, NULL);
	//void* cos;
//SDL_LockTexture(mapTexture, NULL, &mpixels, &formattedSurface->pitch);
//memcpy(mpixels, formattedSurface->pixels, _mapa->pitch * _mapa->h);
	SDL_UpdateTexture(mapTexture, NULL, _mapa->pixels, _mapa->pitch);
	//SDL_UpdateTexture(mTexturesVector[img], NULL, _mapa->pixels, _mapa->pitch);
		//mTexturesVector[img] = SDL_CreateTextureFromSurface(Video::getInstance()->renderer, _mapa);
		//std::cout << "si";
	//SDL_UnlockTexture(mapTexture);
}

void ResourceManager::renderMap()
{
	SDL_Rect* r = new SDL_Rect();
	r->x =Video::getInstance()->getMargeX();
	r->y = 0;
	r->h = 160;
	r->w = 320;
	SDL_Rect*a = new SDL_Rect();
	a->x = 0;
	a->y = 0;
	a->h = 160;
	a->w = 320;
	SDL_RenderCopy(Video::getInstance()->renderer, mapTexture, r, a);
	
}

unsigned int ResourceManager::getPixel(int x, int y)
{
	int bpp = _mapa->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to retrieve */
	if (x>_mapa->w||x<0||y<0||y>_mapa->h)
	{
		return -1;
	}
	Uint8 *p = (Uint8 *)_mapa->pixels + y * _mapa->pitch + x * bpp;

	switch (bpp) {
	case 1:
		return *p;
		break;

	case 2:
		return *(Uint16 *)p;
		break;

	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return p[0] << 16 | p[1] << 8 | p[2];
		else
			return p[0] | p[1] << 8 | p[2] << 16;
		break;

	case 4:
		return *(Uint32 *)p;
		break;

	default:
		return 0;       /* shouldn't happen, but avoids warnings */
	}
	//return res;
}

void ResourceManager::putPixel(int x, int y, unsigned int pix)
{
	SDL_LockSurface(_mapa);
		//Convert the pixels to 32 bit
		Uint8 *pixels = (Uint8 *)_mapa->pixels;
	//Uint32 *pixels2 = (Uint32 *)mpixels;
		//Set the pixel
		Uint8 *p = (Uint8 *)_mapa->pixels + y * _mapa->pitch + x * _mapa->format->BytesPerPixel;

		//pixels[(y * _mapa->pitch) + x*_mapa->format->BytesPerPixel] = pix;
		*(Uint32*)p = pix;
		//pixels2[(y * _mapa->pitch) + x] = pix;
		//SDL_UpdateTexture(mapTexture, NULL, pixels, _mapa->pitch);
		SDL_UnlockSurface(_mapa);
	}

ResourceManager * ResourceManager::getInstance()
{
	if (pInstance == NULL)
		pInstance = new ResourceManager();

	return pInstance;
	
}

ResourceManager::ResourceManager()
{
	
}

Sint32 ResourceManager::addGraphic(const char * file)
{
	Sint32 res;
	if (mIDMap.find(file) != mIDMap.end()) {
		res = mIDMap[file];
	}
	else
	{
		mTexturesVector.push_back(loadTexture(file));
		mIDMap.insert(std::pair<std::string, Sint32>(file, (mTexturesVector.size() - 1)));

		res = mTexturesVector.size() - 1;
	}
	return res;
}

SDL_Texture * ResourceManager::getGraphic(const char * file)
{
	return mTexturesVector[mIDMap[file]];
}

Uint32 ResourceManager::updateFirstFreeSlotGraphic()
{
	for (int i = 0; i < mTexturesVector.size(); i++)
	{
		if (mTexturesVector[i] == NULL) {
			return i;
		}
	}

}


SDL_Texture * ResourceManager::loadTexture(std::string file)
{//Load PNG surface

	std::string path = file; SDL_Surface *surfaceFormatInfo = Video::getInstance()->gScreenSurface;

	

	//The final optimized image
	SDL_Surface* optimizedSurface = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image ", path.c_str(), "! SDL_image Error: ", IMG_GetError());
	}
	else
	{
		//Convert surface to screen format
		// Save Alpha values from surface info
		Uint32 Old_Amask = surfaceFormatInfo->format->Amask;
		Uint32 Old_Aloss = surfaceFormatInfo->format->Aloss;
		Uint32 Old_Ashift = surfaceFormatInfo->format->Ashift;
		// Force Alpha values
		surfaceFormatInfo->format->Amask = 0xFF000000;
		surfaceFormatInfo->format->Aloss = 0;
		surfaceFormatInfo->format->Ashift = 24;
		// Convert to screen format
		optimizedSurface = SDL_ConvertSurface(loadedSurface, surfaceFormatInfo->format, NULL);
		// Restore alpha values for surface info
		surfaceFormatInfo->format->Amask = Old_Amask;
		surfaceFormatInfo->format->Aloss = Old_Aloss;
		surfaceFormatInfo->format->Ashift = Old_Ashift;
		if (optimizedSurface == NULL)
		{
			printf("Unable to optimize image ", path.c_str(), "! SDL Error: ", SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}
	
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Video::getInstance()->renderer, optimizedSurface);
	return tex;
}

SDL_Texture * ResourceManager::loadTexture(std::string file, int x, int y, int h, int w)
{
	std::string path = file ; SDL_Surface *surfaceFormatInfo = Video::getInstance()->gScreenSurface;



	//The final optimized image
	SDL_Surface* optimizedSurface = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image ", path.c_str(), "! SDL_image Error: ", IMG_GetError());
	}
	else
	{
		//Convert surface to screen format
		// Save Alpha values from surface info
		Uint32 Old_Amask = surfaceFormatInfo->format->Amask;
		Uint32 Old_Aloss = surfaceFormatInfo->format->Aloss;
		Uint32 Old_Ashift = surfaceFormatInfo->format->Ashift;
		// Force Alpha values
		surfaceFormatInfo->format->Amask = 0xFF000000;
		surfaceFormatInfo->format->Aloss = 0;
		surfaceFormatInfo->format->Ashift = 24;
		// Convert to screen format
		optimizedSurface = SDL_ConvertSurface(loadedSurface, surfaceFormatInfo->format, NULL);
		// Restore alpha values for surface info
		surfaceFormatInfo->format->Amask = Old_Amask;
		surfaceFormatInfo->format->Aloss = Old_Aloss;
		surfaceFormatInfo->format->Ashift = Old_Ashift;
		if (optimizedSurface == NULL)
		{
			printf("Unable to optimize image ", path.c_str(), "! SDL Error: ", SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}
	//loadedSurface-
	SDL_Texture* tex = SDL_CreateTextureFromSurface(Video::getInstance()->renderer, optimizedSurface);
	return tex;
}

SDL_Texture * ResourceManager::getGraphicByID(Sint32 ID)
{
	return mTexturesVector[ID];
}

Mix_Chunk * ResourceManager::getSoundByID(int ID)
{
	return _mSoundVector[ID];
}

void ResourceManager::setAlphaGraphic(Sint32 ID, Uint8 alpha_value)
{
	SDL_SetTextureAlphaMod(mTexturesVector[ID],alpha_value);
}

void ResourceManager::printLoadedGraphics()
{
	std::string resut;
	for (std::map<std::string, Sint32>::iterator it = mIDMap.begin(); it != mIDMap.end(); ++it) {
		std::cout <<it->first << '\n';
	}
}
