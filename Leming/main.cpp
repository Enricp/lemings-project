#pragma once
#include <iostream>
#include "Video.h"
#include "Punter.h"
#include "ControlManager.h"
#include "Lemming.h"
#include "Boto.h"
#include "Camara.h"

//tileset 16X16 4px de separacio
Video* videproxi = Video::getInstance();
Punter mouse;
ControlManager* controlador = ControlManager::getInstance();
int main(int argc, char* args[])
{
	Boto botonera[12];

	botonera[0].init(RESTA);
	botonera[1].init(SUMA);
	botonera[2].init(ESCALADOR);
	botonera[3].init(PARACAIGUDES);
	botonera[4].init(EXPLOTA);
	botonera[5].init(PARADOR);
	botonera[6].init(CONSTRUCTOR);
	botonera[7].init(PEGADOR);
	botonera[8].init(PICADOR);
	botonera[9].init(CAVADOR);
	botonera[10].init(VELOCITAT);
	botonera[11].init(MUERTE);
	for (int i = 0; i < 12; i++)
	{

		botonera[i].setX(16 * i);
		botonera[i].setY(176);
	}
	Lemming lem;
	std::vector<Lemming*> lems;
	
	lem.init();
	lem.setY(90);
	lem.setX(280);
	lems.push_back(&lem);
	mouse.init();
	videproxi->amagarMouse();
	//lem.setFeina(BASHER);
	Camara cam;
	cam.init();
	videproxi->_rsManager->createMap("tri1.png");
	//int m = videproxi->_rsManager->createTexture("map.png", 320, 200);
	while (!controlador->getExit())
	{

		controlador->UpdateImput();
		int tecla = controlador->getKeyControl();
	
		
		if (tecla>0&&tecla<11)
		{
			for (int i = 0; i < 12; i++)
			{
				botonera[i].setActiu(false);
			}
			if(botonera[tecla].getActiu()){
				botonera[tecla].setActiu(false);
			}
			else
			{
				botonera[tecla].setActiu(true);
			}
		}
		if (tecla==30)
		{
			cam.setX(cam.getX() - 1);

		}
		if (tecla == 20)
		{
			cam.setX(cam.getX() + 1);
		}
		videproxi->setMargeX(cam.getX());
		if (botonera[1].getActiu())
		{
			Lemming* tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
/*
			//a partir d'aqui no serverix
			for (int p = 0; p < 2000000; p++)
			{

			
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
			tempLem = new Lemming();
			tempLem->setX(280);
			tempLem->setY(90);
			lems.push_back(tempLem);
			botonera[1].setActiu(false);
		}*/
		}
		mouse.setX(controlador->getMouseHoritzontalPosition());
		mouse.setY(controlador->getMouseVerticalPosition());
		bool sobre = false;
		for (int i = 0; i < lems.size(); i++)
		{


			for (int b = 0; b < 12; b++)
			{
				if (videproxi->comprovarCollisio(mouse.getCentreX(), mouse.getCentreY(), 1, 1, botonera[b].getX(), botonera[b].getY(), botonera[b].getW(), botonera[b].getH())) {
					if (controlador->getClic())
					{
						for (int a = 0; a < 12; a++)
						{
							botonera[a].setActiu(false);
						}
						botonera[b].setActiu(true);
						botonera[b].setUsos(botonera[b].getUsos() + 1);
					}
				}
				if (lems[i]->getY()<=151)
				{
					//lems.erase(lems.begin()+ i);
				}
			}




			//colisio entre leming i ratoli
			if (videproxi->comprovarCollisio(mouse.getCentreX() + videproxi->getMargeX(), mouse.getCentreY(), 1, 1, lems[i]->getX(), lems[i]->getY(), lems[i]->getW(), lems[i]->getH())) {
				mouse.setEstat(SOBRE);
				sobre = true;
				if (controlador->getClic())
				{
					for (int b = 0; b < 12; b++)
					{
						if (botonera[b].getActiu()) {
							switch (botonera[b].getTipus())
							{
							case SUMA:
								break;
							case RESTA:
								break;
							case ESCALADOR:
								if (!lems[i]->getClimber())
								{
									lems[i]->setClimber(true);
								}
								break;
							case PARACAIGUDES:
								if (!lems[i]->getFloater())
								{
									lems[i]->setFloater(true);
								}
								break;
							case EXPLOTA:
								if (lems[i]->getFeina() == NATURAL)
								{
									lems[i]->setFeina(EXPLOTADOR);
								}
								break;
							case PARADOR:
								if (lems[i]->getFeina() == NATURAL)
								{
									lems[i]->setFeina(BLOCKER);
									for (int h = lems[i]->getY() - 2; h < lems[i]->getY() + lems[i]->getH() - 7; h++)
									{
										for (int j = lems[i]->getX()+4; j < lems[i]->getX() + lems[i]->getW() - 2; j++)
										{
											videproxi->_rsManager->putPixel(j, h, 0xFF000000);
										}

									}
									videproxi->_rsManager->updateMap(0);
								}
								break;
							case CONSTRUCTOR:
								if (lems[i]->getFeina() == NATURAL)
								{
									lems[i]->setFeina(BUILDER);
								}
								break;
							case PEGADOR:
								if (lems[i]->getFeina() == NATURAL)
								{
									lems[i]->setFeina(BASHER);
								}
								break;
							case PICADOR:
								if (lems[i]->getFeina() == NATURAL)
								{
									lems[i]->setFeina(MINER);
								}
								break;
							case CAVADOR:
								if (lems[i]->getFeina() == NATURAL)
								{
									lems[i]->setFeina(DIGGER);
								}
								break;
							case VELOCITAT:
								break;
							case MUERTE:
								break;
							default:
								break;
							}
						}
					}
				}
			}
			if (sobre == false)
			{
				mouse.setEstat(NORMAL);
			}


			


			lems[i]->update();
		}
		videproxi->clearScreen(255);
		
		//videproxi->renderTexture(m, 0, 0, 320, 200);
		videproxi->_rsManager->renderMap();
		for (int i = 0; i < lems.size(); i++)
		{
			lems[i]->render();
		}
		for (int i = 0; i < 12; i++)
		{
			botonera[i].render();
		}
		mouse.render();

		videproxi->updateScreen();
	}

	videproxi->close();
	return 0;
}