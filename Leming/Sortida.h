#pragma once
class Sortida
{

	int _x;
	int _y;
	int _h;
	int _w;
public:
	Sortida();
	~Sortida();

	int getX();
	int getY();
	int getH();
	int getW();

	void setX(int novaX);
	void setY(int novaY);
	void setW(int novaW);
	void setH(int novaH);
};

