#include "IconaMenu.h"



IconaMenu::IconaMenu()
{
}


IconaMenu::~IconaMenu()
{
}

void IconaMenu::setImg(int novaImg)
{
	_img = novaImg;
}

void IconaMenu::setX(int novaX)
{
	_x = novaX;
}

void IconaMenu::setY(int novaY)
{
	_y = novaY;
}

void IconaMenu::setH(int novaH)
{
	_h = novaH;
}

void IconaMenu::setW(int novaW)
{
	_w = novaW;
}

void IconaMenu::setTipus(tipusBotoMenu nouTipus)
{
	_tipus = nouTipus;
}

int IconaMenu::getImg()
{
	return _img;
}

int IconaMenu::getX()
{
	return _x;
}

int IconaMenu::getY()
{
	return _y;
}

int IconaMenu::getH()
{
	return _h;
}

int IconaMenu::getW()
{
	return _w;
}

tipusBotoMenu IconaMenu::getTipus()
{
	return _tipus;
}

void IconaMenu::init()
{
	_x = 0;
	_y = 0;
	_w = 120;
	_h = 63;
	_videoManager = Video::getInstance();

}

void IconaMenu::init(tipusBotoMenu tipus)
{
	_x = 0;
	_y = 0;
	_w = 120;
	_h = 63;
	_videoManager = Video::getInstance();
	_tipus = tipus;
	switch (_tipus)
	{
	case SORTIR:
		_img = _videoManager->_rsManager->createTexture("sign_quit.png", 120, 63, 0, 0);
		break;
	case JUGAR:
		_img = _videoManager->_rsManager->createTexture("sign_play.png", 120, 63, 0, 0);
		break;
	default:
		break;
	}
}

void IconaMenu::render()
{
	_videoManager->renderTextureSO(_img, _x, _y, _w, _h, 0, 0);
}
