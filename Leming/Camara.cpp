#include "Camara.h"



Camara::Camara()
{
}


Camara::~Camara()
{
}

void Camara::setX(int novaX)
{
	_x = novaX;
}

void Camara::setY(int novaY)
{
	_y = novaY;
}

void Camara::setW(int novaW)
{
	_w = novaW;
}

void Camara::setH(int novaH)
{
	_h = novaH;
}

int Camara::getX()
{
	return _x;
}

int Camara::getY()
{
	return _y;
}

int Camara::getH()
{
	return _h;
}

int Camara::getW()
{
	return _w;
}

void Camara::init()
{
	_x = 0;
	_y = 0;
	_w = 320;
	_h = 160;
}
